<?php

/**
 * Class PDO_DB
 */
final class PDO_DB
{

  private static $db = null;

  /**
   * ����������� ������
   */
  function __construct()
  {

  }

  /**
   * @return PDO - ������� ����������� � ���������� ������ PDO
   */
  public static function getConnection()
  {
    if (self::$db == null) {

      $opt = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => TRUE,
      );

      $paramsPath = ROOT . '\config\db_params.php';
      $params = include($paramsPath);

      $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";

      $db = new PDO($dsn, $params['user'], $params['password'], $opt);

      $db->exec("set names utf8");
    }

    return $db;   // ���������� ������ PDO
  }

}
